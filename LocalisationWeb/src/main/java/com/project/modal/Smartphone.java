package com.project.modal;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Smartphone implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1326448912917383902L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String imei;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	

	public Smartphone(String imei) {
		super();
		this.imei = imei;
	}
	
	public Smartphone() {
		super();
	}
	
	

	public Smartphone(Long id) {
		super();
		this.id = id;
	}

	public Smartphone(String imei, User user) {
		super();
		this.imei = imei;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return imei ;
	}
	
	
}
