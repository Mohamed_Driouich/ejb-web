<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="script/jquery-3.5.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>



<body>
	<fieldset>
		<legend> Informations User </legend>
		<form action="ControllerServeltSmartphone" method="post">
		<input type="hidden" name="action" value="add" />
		 <div class="form-group">
    <label for="exampleInputEmail1">imei</label>
    <input type="text" class="form-control" name="imei" placeholder="Enter imei">
  </div>
   <div class="form-group">
    <select name="user" class="form-select" aria-label="Default select example">
  <option selected>choose user</option>
   <% int i=0; %>
    <c:forEach items="${users}" var="a">
  <option  value="${i+1 }">${ a.getNom() }</option>
  </c:forEach>
</select>
  </div>
   
 
  <input type="submit" value="add" class="btn btn-primary"/>
	</fieldset>
</form>
	<fieldset>
		<legend> Liste des Smartphone </legend>
		<table border="1" class="table table-dark">
			<thead>
				<tr>
					<th>imei</th>
					<th>User</th>
					<th>Supprimer</th>
					<th>Modifier</th>
				</tr>
				<c:forEach items="${smartphones}" var="c">
				<tr>
					<th>${c.getImei() }</th>
					<th>${c.getUser() }</th>
					<th> <a href="SmartphoneController?action=delete/${c.getId()}" class="btn btn-danger">delete</a></th>
					<th>
					<a href="editUser.jsp?action=editUser&userId=${c.getId()}" class="btn bnt-primary">Edit</a>
				
					</th>
				</tr>
				</c:forEach>
			</thead>
			<tbody id="content">
			</tbody>
		</table>
		
		
</body>
</html>