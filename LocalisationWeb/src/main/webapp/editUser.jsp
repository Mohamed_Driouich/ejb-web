<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script src="script/jquery-3.5.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<fieldset>
		<legend> edit User </legend>
		<form action="UserController" method="post">
		<input type="hidden" name="action" value="update" />
		
		 <div class="form-group">
    <label for="exampleInputEmail1">Nom</label>
    <input type="text" class="form-control" name="nom" value="${user.getNom() }" placeholder="Enter name">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">prenom</label>
    <input type="text" class="form-control" name="prenom" value="${user.getPrenom() }" placeholder="Enter prenom">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Tele</label>
    <input type="number" class="form-control" name="tele" value="${user.getTele() }"  placeholder="Enter tele">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" value="${user.getEmail() }" aria-describedby="emailHelp" placeholder="Enter email">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Date Naissance</label>
    <input type="Date" class="form-control" name="dateNaiss" value="${user.getDateNaiss() }"  placeholder="Enter Date Naissance">
  </div>
 
  <input type="submit" value="update" class="btn btn-primary"/>
	</fieldset>
</form>
	<fieldset>
</body>
</html>